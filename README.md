#Automation Framework

Automation framework developed as technical solution for LeasePlan project interview process.

### Requirements
* APIs: https://developer.nytimes.com/docs/most-popular-product/1/overview / https://developer.nytimes.com/docs/movie-reviews-api/1/overview
* CI platform: GitLab CI
* BDD format: Cucumber/Gherkin
* Framework: Java

### To implement
* Automate the endpoints inside a working CI/CD pipeline to test the service
* Cover at least 1 positive and 1 negative scenario
* Set Html reporting tool with the test results of the CI/CD test run
* Gitlab link: https://gitlab.com/EugenS21/automation-framework

### Used Tools
* java >= 11
* maven - 3.6
* spring boot - 2.7.0-SNAPSHOT
* cucumber - 7.2.3
* lombok - 1.18.22
* vavr - 0.10.4

### How to play
Run tests via maven command.
Execute `mvn clean verify -P=cucumber` to start test from CLI locally.

### Testing on GitLab
When a merge request is created a pipeline will run, to check the build and generate a report in **Tests** section on pipeline results page.
When a branch is merged - no tests are generated.