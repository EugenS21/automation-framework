package org.lease_plan.client.model;

import static lombok.AccessLevel.PRIVATE;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor(force = true)
@FieldDefaults(makeFinal = true, level = PRIVATE)
@Data
public class EmailedArticleResponse {

    String status;
    String copyright;
    Integer numResults;
    List<EmailArticleDto> results;

}
