package org.lease_plan.client.model;

import static lombok.AccessLevel.PRIVATE;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Builder (toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor (force = true)
@FieldDefaults (makeFinal = true, level = PRIVATE)
@Data
public class MediaMetadataDto {

    String url;
    String format;
    Integer height;
    Integer width;

}
