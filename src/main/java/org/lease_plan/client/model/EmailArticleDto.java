package org.lease_plan.client.model;

import static lombok.AccessLevel.PRIVATE;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Builder (toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor (force = true)
@FieldDefaults (makeFinal = true, level = PRIVATE)
@Data
public class EmailArticleDto {

    String url;
    String uri;
    Long id;
    Long assetId;
    String source;
    String publishedDate;
    String updated;
    String section;
    String subsection;
    String nytdsection;
    String adxKeywords;
    String column;
    String byline;
    String type;
    String title;
    @JsonProperty("abstract")
    String abstractTitle;
    List<String> desFacet;
    List<String> orgFacet;
    List<String> perFacet;
    List<String> geoFacet;
    List<MediaDto> media;

}
