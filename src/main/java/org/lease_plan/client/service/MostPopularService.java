package org.lease_plan.client.service;

import com.jcabi.aspects.Loggable;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.lease_plan.client.model.EmailedArticleResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@Service
@Slf4j
public class MostPopularService {

    WebClient webClient;
    String apiKey;

    @Autowired
    public MostPopularService(
        WebClient webClient,
        @Value("${new_york_times_base_url}") String baseUrl,
        @Value("${most_popular}") String mostPopularApi,
        @Value("${api_key}") String apiKey
    ) {
        this.apiKey = apiKey;
        this.webClient = webClient.mutate()
            .baseUrl(baseUrl.concat(mostPopularApi))
            .build();
    }

    @Loggable
    public ResponseEntity<EmailedArticleResponse> getEmailedArticle() {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder.path("/emailed/7.json")
                .queryParam("api-key", apiKey)
                .build())
            .retrieve()
            .toEntity(EmailedArticleResponse.class)
            .block();
    }

    @Loggable
    public ResponseEntity<EmailedArticleResponse> getEmailedArticleWithoutApiKey() {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder.path("/emailed/7.json").build())
            .retrieve()
            .toEntity(EmailedArticleResponse.class)
            .block();
    }


}
