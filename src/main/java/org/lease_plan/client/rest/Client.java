package org.lease_plan.client.rest;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

@Configuration
@Slf4j
public class Client {

    private ReactorClientHttpConnector getHttpClientWithTimeouts() {
        HttpClient httpClient = HttpClient.create()
            .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
            .responseTimeout(Duration.ofMillis(5000))
            .wiretap(true)
            .doOnConnected(conn -> conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
                .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));
        return new ReactorClientHttpConnector(httpClient);
    }

    private ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(request -> {
            log.info("Request to ====> " + request.url());
            if (log.isDebugEnabled()) {
                StringBuilder stringBuilder = new StringBuilder("Headers: \n");
                request
                    .headers()
                    .forEach((name, values) -> values.forEach(stringBuilder::append));
                log.debug(stringBuilder.toString());
            }
            return Mono.just(request);
        });
    }

    private ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(response -> {
            log.info("Response   <==== status: " + response.statusCode().value());
            return Mono.just(response);
        });
    }

    @Bean
    public WebClient webClient(
        @Value ("${new_york_times_base_url}") String baseUrl
    ) {
        return WebClient.builder()
            .baseUrl(baseUrl)
            .clientConnector(getHttpClientWithTimeouts())
            .defaultHeader("Accept", "application/json, text/plain, */*")
            .filters(exchangeFilterFunctions -> {
                exchangeFilterFunctions.add(logRequest());
                exchangeFilterFunctions.add(logResponse());
            })
            .build();
    }

}
