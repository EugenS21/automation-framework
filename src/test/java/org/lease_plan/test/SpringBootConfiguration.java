package org.lease_plan.test;

import io.cucumber.spring.CucumberContextConfiguration;
import org.lease_plan.test.configuration.SpringConfigurations;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = SpringConfigurations.class, properties ="classpath:application.properties")
public class SpringBootConfiguration {}
