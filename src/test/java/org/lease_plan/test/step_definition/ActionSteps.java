package org.lease_plan.test.step_definition;

import static org.lease_plan.test.enums.StorageKey.RESPONSE;
import static org.lease_plan.test.enums.StorageKey.RESPONSE_BODY;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.lease_plan.client.model.EmailedArticleResponse;
import org.lease_plan.client.service.MostPopularService;
import org.lease_plan.test.configuration.ScenarioContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ActionSteps {

    MostPopularService mostPopularService;
    ScenarioContext scenarioContext;

    @Given ("sent a request to get the most popular articles sent via email")
    public void userSentARequestToGetTheMostPopularArticlesSentViaEmail() {
        ResponseEntity<EmailedArticleResponse> emailedArticleResponse = mostPopularService.getEmailedArticle();
        scenarioContext.saveObject(RESPONSE, emailedArticleResponse);
    }

    @Given ("sent a request to get the most popular articles sent via email without token")
    public void userSentARequestToGetTheMostPopularArticlesSentViaEmailWithoutToken() {
        ResponseEntity<EmailedArticleResponse> emailedArticleResponse = mostPopularService.getEmailedArticleWithoutApiKey();
        scenarioContext.saveObject(RESPONSE, emailedArticleResponse);
    }

    @SuppressWarnings("all")
    @When ("extract body from the response")
    public void extractBodyFromTheResponse() {
        ResponseEntity responseEntity = scenarioContext.getObject(RESPONSE, ResponseEntity.class);
        scenarioContext.saveObject(RESPONSE_BODY, responseEntity.getBody());
    }
}
