package org.lease_plan.test.step_definition;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.spring.ScenarioScope;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.val;
import org.assertj.core.api.SoftAssertions;
import org.lease_plan.client.model.EmailedArticleResponse;
import org.lease_plan.test.configuration.ScenarioContext;
import org.lease_plan.test.constant.TestConstants;
import org.lease_plan.test.enums.StorageKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor (onConstructor = @__(@Autowired))
public class AssertionSteps {

    ScenarioContext scenarioContext;
    SoftAssertions softAssertions;

    @When ("should receive status code '{}'")
    @SuppressWarnings("all")
    public void shouldReceiveStatusCode(int statusCode) {
        ResponseEntity response = scenarioContext.getObject(StorageKey.RESPONSE, ResponseEntity.class);
        softAssertions.assertThat(response)
            .describedAs("Unexpected response status code")
            .extracting(el->el.getStatusCodeValue())
            .isEqualTo(statusCode);
    }


    @Then ("validate most popular articles sent via email")
    public void validateMostPopularArticlesSentViaEmail() {
        var response = scenarioContext.getObject(StorageKey.RESPONSE_BODY, EmailedArticleResponse.class);
        softAssertions.assertThat(response)
            .describedAs("Unexpected response details")
            .satisfies(emailedArticleResponse -> softAssertions.assertThat(emailedArticleResponse.getStatus()).isEqualTo("OK"))
            .extracting(EmailedArticleResponse::getResults)
            .satisfies(results -> softAssertions.assertThat(results).hasSize(20))
            .asList()
            .extracting("url")
            .doesNotContainNull()
            .allSatisfy(url -> softAssertions.assertThat(url.toString()).matches(TestConstants.URL_PATTERN));
    }
}
