package org.lease_plan.test.configuration;

import io.cucumber.spring.ScenarioScope;
import java.util.HashMap;
import java.util.Map;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.lease_plan.test.enums.StorageKey;
import org.springframework.stereotype.Component;

@Component
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ScenarioContext {

    Map<StorageKey, Object> mapToStore;

    public ScenarioContext() {
        this.mapToStore = new HashMap<>();
    }

    public <T> T getObject(StorageKey storageKey, Class<T> clazz) {
        return clazz.cast(mapToStore.get(storageKey));
    }

    public void saveObject(StorageKey storageKey, Object object) {
        mapToStore.put(storageKey, object);
    }

}
