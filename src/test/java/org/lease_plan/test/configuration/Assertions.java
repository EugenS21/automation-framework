package org.lease_plan.test.configuration;

import org.assertj.core.api.SoftAssertions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Assertions {

    @Bean
    public SoftAssertions getSoftAssertions() {
        return new SoftAssertions();
    }

}
