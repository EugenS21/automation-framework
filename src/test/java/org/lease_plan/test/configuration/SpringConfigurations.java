package org.lease_plan.test.configuration;

import org.lease_plan.client.AutomationFrameworkApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(AutomationFrameworkApplication.class)
@ComponentScan("org.lease_plan.test")
public class SpringConfigurations {}