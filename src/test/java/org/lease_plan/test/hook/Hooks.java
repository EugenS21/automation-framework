package org.lease_plan.test.hook;

import io.cucumber.spring.ScenarioScope;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.jupiter.api.AfterAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@FieldDefaults (makeFinal = true, level = AccessLevel.PRIVATE)
@RequiredArgsConstructor (onConstructor = @__(@Autowired))
public class Hooks {

    SoftAssertions softAssertions;

    @AfterAll
    public void doSoftAssert() {
        softAssertions.assertAll();
    }

}
