package org.lease_plan.test.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TestConstants {

    public String URL_PATTERN = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";

}
